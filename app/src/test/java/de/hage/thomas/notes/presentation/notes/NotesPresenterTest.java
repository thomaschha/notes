package de.hage.thomas.notes.presentation.notes;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import de.hage.thomas.notes.data.ContractRepository;
import de.hage.thomas.notes.model.Note;
import de.hage.thomas.notes.presentation.UserConfirmation;

import static org.mockito.Mockito.*;

/**
 * Created by tomes on 14.11.17.
 */
@SuppressWarnings("DefaultFileTemplate")
public class NotesPresenterTest {

    @Captor
    private ArgumentCaptor<ContractRepository.DefaultCallback<List<de.hage.thomas.notes.model.Note>>> defaultCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<UserConfirmation> userConfirmationArgumentCaptor;

    @Mock
    private NotesContract.View activityView;

    @Mock
    private NotesContract.View.Overview overview;

    @Mock
    private NotesContract.View.Bin binView;

    @Mock
    private ContractRepository.Notes notesRepository;

    private NotesContract.Presenter presenter;

    @Before
    public void setUp() throws Exception {
        // init objects with mockito annotations
        MockitoAnnotations.initMocks(this);

        presenter = new NotesPresenter(
                notesRepository,
                activityView,
                overview,
                binView
        );

        verify(overview).setPresenter(presenter);
        verify(binView).setPresenter(presenter);
    }

    @Test
    public void testLoadingActiveNotesSuccessfully(){
        // given
        List<de.hage.thomas.notes.model.Note> notes = new ArrayList<>();
        notes.add(mock(de.hage.thomas.notes.model.Note.class));
        notes.add(mock(de.hage.thomas.notes.model.Note.class));
        notes.add(mock(de.hage.thomas.notes.model.Note.class));
        notes.add(mock(de.hage.thomas.notes.model.Note.class));

        // when
        presenter.loadActiveNotes();

        // then
        verify(overview).hideEmptyContentMessage();
        verify(overview).hideRecyclerView();
        verify(overview).showLoadingProgressBar();
        verify(notesRepository).resumeOpenTaskFragment(defaultCallbackArgumentCaptor.capture());
        defaultCallbackArgumentCaptor.getValue().onSuccess(notes);
        verifyNoMoreInteractions(notesRepository);

        verify(overview).hideLoadingProgressBar();
        verify(overview).showRecyclerView();
        verify(overview).onOpenNotesLoaded(notes);
        verifyNoMoreInteractions(overview);
    }

    @Test
    public void testLoadingActiveNotesEmpty(){
        // given
        List<de.hage.thomas.notes.model.Note> emptryNotes = new ArrayList<>();

        // when
        presenter.loadActiveNotes();

        // then
        verify(overview).hideEmptyContentMessage();
        verify(overview).hideRecyclerView();
        verify(overview).showLoadingProgressBar();
        verify(notesRepository).resumeOpenTaskFragment(defaultCallbackArgumentCaptor.capture());
        defaultCallbackArgumentCaptor.getValue().onSuccess(emptryNotes);
        verifyNoMoreInteractions(notesRepository);

        verify(overview).hideLoadingProgressBar();
        verify(overview).showEmptyContentMessage();
        verifyNoMoreInteractions(overview);
    }

    @Test
    public void testLoadingActiveNotesWithError(){
        // when
        presenter.loadActiveNotes();

        // then
        verify(overview).hideRecyclerView();
        verify(overview).hideEmptyContentMessage();
        verify(overview).showLoadingProgressBar();
        verify(notesRepository).resumeOpenTaskFragment(defaultCallbackArgumentCaptor.capture());
        defaultCallbackArgumentCaptor.getValue().onError();
        verifyNoMoreInteractions(notesRepository);

        verify(overview).hideLoadingProgressBar();
        verify(overview).showEmptyContentMessage();
        verifyNoMoreInteractions(overview);
    }

    @Test
    public void testLoadingFinishedNotesSuccessfully(){
        // given
        List<de.hage.thomas.notes.model.Note> notes = new ArrayList<>();
        notes.add(mock(de.hage.thomas.notes.model.Note.class));
        notes.add(mock(de.hage.thomas.notes.model.Note.class));
        notes.add(mock(de.hage.thomas.notes.model.Note.class));
        notes.add(mock(de.hage.thomas.notes.model.Note.class));

        // when
        presenter.loadFinishedNotes();

        // then
        verify(binView).hideRecyclerView();
        verify(binView).hideEmptyContentMessage();
        verify(binView).showLoadingProgressBar();
        verify(notesRepository).resumeDoneTaskFragment(defaultCallbackArgumentCaptor.capture());
        defaultCallbackArgumentCaptor.getValue().onSuccess(notes);
        verifyNoMoreInteractions(notesRepository);

        verify(binView).hideLoadingProgressBar();
        verify(binView).showRecyclerView();
        verify(binView).onDoneNotesLoaded(notes);
        verifyNoMoreInteractions(binView);
    }

    @Test
    public void testLoadingFinishedNotesEmpty(){
        // given
        List<de.hage.thomas.notes.model.Note> emptyNotes = new ArrayList<>();

        // when
        presenter.loadFinishedNotes();

        // then
        verify(binView).hideEmptyContentMessage();
        verify(binView).hideRecyclerView();
        verify(binView).showLoadingProgressBar();
        verify(notesRepository).resumeDoneTaskFragment(defaultCallbackArgumentCaptor.capture());
        defaultCallbackArgumentCaptor.getValue().onSuccess(emptyNotes);
        verifyNoMoreInteractions(notesRepository);

        verify(binView).hideLoadingProgressBar();
        verify(binView).showEmptyContentMessage();
        verifyNoMoreInteractions(binView);
    }

    @Test
    public void testLoadingFinishedNotesWithError(){
        // when
        presenter.loadFinishedNotes();

        // then
        verify(binView).hideEmptyContentMessage();
        verify(binView).hideRecyclerView();
        verify(binView).showLoadingProgressBar();
        verify(notesRepository).resumeDoneTaskFragment(defaultCallbackArgumentCaptor.capture());
        defaultCallbackArgumentCaptor.getValue().onError();
        verifyNoMoreInteractions(notesRepository);

        verify(binView).hideLoadingProgressBar();
        verify(binView).showEmptyContentMessage();
        verifyNoMoreInteractions(binView);
    }

    @Test
    public void testEmptyBinWithEmptyList(){
        // given
        when(binView.hasItems()).thenReturn(false);

        // when
        presenter.emptyBin();

        // then
        verify(binView).hasItems();
        verifyNoMoreInteractions(binView);
        verify(activityView).showNothingToRecycleToast();
        verifyNoMoreInteractions(activityView);
    }

    @Test
    public void testRecycleBinWithNotesAndAcceptance(){
        // given
        when(binView.hasItems()).thenReturn(true);

        // when
        presenter.emptyBin();

        // then
        verify(binView).hasItems();
        verify(activityView).showEmptyBinDialog(userConfirmationArgumentCaptor.capture());
        userConfirmationArgumentCaptor.getValue().confirmed();
        verifyNoMoreInteractions(activityView);
        verify(notesRepository).deleteDoneNotesFromDatabase();
        verifyNoMoreInteractions(notesRepository);
        verify(binView).removeAllNotes();
        verify(binView).hideRecyclerView();
        verify(binView).hideLoadingProgressBar();
        verify(binView).showEmptyContentMessage();
        verifyNoMoreInteractions(binView);
    }

    @Test
    public void testRecycleBinWithNotesButUserDeclined(){
        // given
        when(binView.hasItems()).thenReturn(true);

        // when
        presenter.emptyBin();

        // then
        verify(binView).hasItems();
        verifyNoMoreInteractions(binView);
        verify(activityView).showEmptyBinDialog(userConfirmationArgumentCaptor.capture());
        userConfirmationArgumentCaptor.getValue().declined();
        verifyNoMoreInteractions(activityView);
        verifyZeroInteractions(notesRepository);
    }

    @Test
    public void testRequestFragmentForNewNote(){
        // when
        presenter.requestFragmentNewNote();

        // then
        verify(activityView).showNoteFragmentForNewNote();
        verifyNoMoreInteractions(activityView);
    }

    @Test
    public void testSetNoteDoneAndItemsExistsInRecyclerViewAndOverviewRemainsNotEmpty(){
        // given
        Note mockedNote = mock(Note.class);
        when(binView.hasItems()).thenReturn(true);
        when(overview.hasItems()).thenReturn(true);
        // when
        presenter.setNoteDone(mockedNote);

        // then
        verify(binView).hasItems();
        verify(notesRepository).markForBin(eq(mockedNote));
        verify(binView).addDoneNote(eq(mockedNote));
        verify(overview).removeDoneNote(eq(mockedNote));
        verify(overview).hasItems();
        verifyNoMoreInteractions(notesRepository);
        verifyNoMoreInteractions(binView);
        verifyNoMoreInteractions(overview);
    }

    @Test
    public void testSetNoteDoneAndItemsExistsInRecyclerViewButOverviewIsNowEmpty(){
        // given
        Note mockedNote = mock(Note.class);
        when(binView.hasItems()).thenReturn(true);
        when(overview.hasItems()).thenReturn(false);
        // when
        presenter.setNoteDone(mockedNote);

        // then
        verify(binView).hasItems();
        verify(notesRepository).markForBin(eq(mockedNote));
        verify(binView).addDoneNote(eq(mockedNote));
        verify(overview).removeDoneNote(eq(mockedNote));
        verify(overview).hasItems();
        verify(overview).hideRecyclerView();
        verify(overview).showEmptyContentMessage();
        verifyNoMoreInteractions(notesRepository);
        verifyNoMoreInteractions(binView);
        verifyNoMoreInteractions(overview);
    }

    @Test
    public void testSetNoteDoneAndNoItemsInRecyclerViewAndOverviewRemainsWithItem(){
        // given
        Note mock = mock(Note.class);
        when(binView.hasItems()).thenReturn(false);
        when(overview.hasItems()).thenReturn(true);
        // when
        presenter.setNoteDone(mock);

        // then
        verify(binView).hasItems();
        verify(binView).hideLoadingProgressBar();
        verify(binView).hideEmptyContentMessage();
        verify(binView).showRecyclerView();
        verify(notesRepository).markForBin(eq(mock));
        verify(binView).addDoneNote(eq(mock));
        verify(overview).removeDoneNote(eq(mock));
        verify(overview).hasItems();
        verifyNoMoreInteractions(binView);
        verifyNoMoreInteractions(notesRepository);
        verifyNoMoreInteractions(overview);
    }

    @Test
    public void testSetNoteDoneAndNoItemsInRecyclerViewButOverviewIsEmptyNow(){
        // given
        Note mock = mock(Note.class);
        when(binView.hasItems()).thenReturn(false);
        when(overview.hasItems()).thenReturn(false);

        // when
        presenter.setNoteDone(mock);

        // then
        verify(binView).hasItems();
        verify(binView).hideLoadingProgressBar();
        verify(binView).hideEmptyContentMessage();
        verify(binView).showRecyclerView();
        verify(notesRepository).markForBin(eq(mock));
        verify(binView).addDoneNote(eq(mock));
        verify(overview).removeDoneNote(eq(mock));
        verify(overview).hasItems();
        verify(overview).hideRecyclerView();
        verify(overview).showEmptyContentMessage();
        verifyNoMoreInteractions(binView);
        verifyNoMoreInteractions(notesRepository);
        verifyNoMoreInteractions(overview);
    }

    @Test
    public void testAddNoteToOverviewAndRecyclerHasItems(){
        // given
        Note note = mock(Note.class);
        when(overview.hasItems()).thenReturn(true);

        // when
        presenter.addNote(note);

        // then
        verify(overview).hasItems();
        verify(notesRepository).addNote(eq(note));
        verifyNoMoreInteractions(notesRepository);
        verify(overview).addOpenNote(eq(note));
        verifyNoMoreInteractions(overview);
    }

    @Test
    public void testAddNoteToOverviewAndRecyclerHasNoItems(){
        // given
        Note mocked = mock(Note.class);
        when(overview.hasItems()).thenReturn(false);

        // when
        presenter.addNote(mocked);

        // then
        verify(overview).hasItems();
        verify(overview).hideEmptyContentMessage();
        verify(overview).hideLoadingProgressBar();
        verify(overview).showRecyclerView();
        verify(notesRepository).addNote(eq(mocked));
        verify(overview).addOpenNote(eq(mocked));
        verifyNoMoreInteractions(overview);
        verifyNoMoreInteractions(notesRepository);
    }

    @Test
    public void testRequestDeleteDialog(){
        // given
        UserConfirmation callback = mock(UserConfirmation.class);

        // when
        presenter.requestDeleteDialog(callback);

        // then
        verify(activityView).showNoteDeleteDialog(eq(callback));
        verifyNoMoreInteractions(activityView);
    }

    @Test
    public void testSetNoteOpenAndRecyclerHasItemsAndBinHasAlsoItemsLeft(){
        // given
        Note mockedNote = mock(Note.class);
        when(overview.hasItems()).thenReturn(true);
        when(binView.hasItems()).thenReturn(true);

        // when
        presenter.setNoteOpen(mockedNote);

        // then
        verify(overview).hasItems();
        verify(notesRepository).setNoteOpen(eq(mockedNote));
        verifyNoMoreInteractions(notesRepository);
        verify(overview).addOpenNote(eq(mockedNote));
        verify(binView).onNoteRestored(eq(mockedNote));
        verify(binView).hasItems();
        verifyNoMoreInteractions(binView);
        verifyNoMoreInteractions(overview);
    }

    @Test
    public void testSetNoteOpenAndRecyclerHasItemsButBinIsEmptyNow(){
        // given
        Note mockedNote = mock(Note.class);
        when(overview.hasItems()).thenReturn(true);
        when(binView.hasItems()).thenReturn(false);

        // when
        presenter.setNoteOpen(mockedNote);

        // then
        verify(overview).hasItems();
        verify(notesRepository).setNoteOpen(eq(mockedNote));
        verifyNoMoreInteractions(notesRepository);
        verify(overview).addOpenNote(eq(mockedNote));
        verify(binView).onNoteRestored(eq(mockedNote));
        verify(binView).hasItems();
        verify(binView).hideRecyclerView();
        verify(binView).showEmptyContentMessage();
        verifyNoMoreInteractions(binView);
        verifyNoMoreInteractions(overview);
    }


    @Test
    public void testSetNoteOpenAndRecyclerViewHasNoItemsAndBinHasStillAnItemAdded(){
        // given
        Note mocked = mock(Note.class);
        when(binView.hasItems()).thenReturn(true);
        when(overview.hasItems()).thenReturn(false);

        // when
        presenter.setNoteOpen(mocked);

        // then
        verify(overview).hasItems();
        verify(overview).hideEmptyContentMessage();
        verify(overview).hideLoadingProgressBar();
        verify(overview).showRecyclerView();
        verify(notesRepository).setNoteOpen(eq(mocked));
        verify(overview).addOpenNote(eq(mocked));
        verify(binView).onNoteRestored(eq(mocked));
        verify(binView).hasItems();
        verifyNoMoreInteractions(notesRepository);
        verifyNoMoreInteractions(overview);
        verifyNoMoreInteractions(binView);
    }

    @Test
    public void testSetNoteOpenAndRecyclerViewHasNoItemsButBinIsNowAlsoEmpty(){
        // given
        Note mocked = mock(Note.class);
        when(overview.hasItems()).thenReturn(false);
        when(binView.hasItems()).thenReturn(false);

        // when
        presenter.setNoteOpen(mocked);

        // then
        verify(overview).hasItems();
        verify(overview).hideEmptyContentMessage();
        verify(overview).hideLoadingProgressBar();
        verify(overview).showRecyclerView();
        verify(notesRepository).setNoteOpen(eq(mocked));
        verify(overview).addOpenNote(eq(mocked));
        verify(binView).onNoteRestored(eq(mocked));
        verify(binView).hasItems();
        verify(binView).hideRecyclerView();
        verify(binView).showEmptyContentMessage();
        verifyNoMoreInteractions(notesRepository);
        verifyNoMoreInteractions(overview);
        verifyNoMoreInteractions(binView);
    }

    @Test
    public void testSelectingNote(){
        // given
        Note selectedNote = mock(Note.class);

        // when
        presenter.onNoteSelected(selectedNote);

        // then
        verify(activityView).showNoteFragmentForNote(eq(selectedNote));
        verifyNoMoreInteractions(activityView);
    }

    @Test
    public void testSetFiltering(){
        // given
        CharSequence charSequence = "4711";

        // when
        presenter.setFiltering(charSequence);

        // then
        verify(overview).applyFilter(eq(charSequence));
        verify(binView).applyFilter(eq(charSequence));
        verifyNoMoreInteractions(overview);
        verifyNoMoreInteractions(binView);
    }
}
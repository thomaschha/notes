package de.hage.thomas.notes.presentation.notes.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import de.hage.thomas.notes.R;
import de.hage.thomas.notes.model.Note;
import de.hage.thomas.notes.presentation.UserConfirmation;
import de.hage.thomas.notes.presentation.notes.NotesContract;

/**
 * Created by tomes on 14.11.17.
 */
@SuppressWarnings("DefaultFileTemplate")
public class NoteFragment extends Fragment {

    public static final String BUNDLE_KEY_OPENED_NOTE = "BUNDLE_NOTE";

    private NotesContract.Presenter presenter;

    private Note mNote;

    private ViewGroup dateContainer;
    private TextView dateTextView;
    private AutoCompleteTextView editTextMemo;
    private EditText editTextTitle;
    private FloatingActionButton deleteFAB;
    private boolean delete;

    public static NoteFragment getInstance() {
        return new NoteFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_note, container, false);
        dateContainer = v.findViewById(R.id.fragment_edit_date_container);
        dateTextView = v.findViewById(R.id.fragment_edit_date);
        editTextMemo = v.findViewById(R.id.fragment_edit_edittext_memo);
        editTextTitle = v.findViewById(R.id.fragment_edit_edittext_title);
        deleteFAB = v.findViewById(R.id.fragment_edit_fab_delete);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        deleteFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteNote();
            }
        });
        Bundle args = getArguments();
        if (args != null) {
            fillWithData(args);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!delete) saveNote();
    }

    private void fillWithData(Bundle args) {
        mNote = args.getParcelable(BUNDLE_KEY_OPENED_NOTE);
        if (mNote != null) {
            // since the Note is within our storage, we can delete it:
            deleteFAB.setVisibility(View.VISIBLE);

            // parse the given data to the inputFields
            editTextTitle.setText(mNote.getTitle());

            String lastEdit = mNote.getLastEditedText();
            if (lastEdit != null) {
                dateContainer.setVisibility(View.VISIBLE);
                dateTextView.setText(lastEdit);
            }
            editTextMemo.setText(mNote.getMemoText());
        }
    }

    private void deleteNote() {
        delete = true;
        presenter.requestDeleteDialog(new UserConfirmation() {
            @Override
            public void confirmed() {
                presenter.setNoteDone(getNote());
                getActivity().getSupportFragmentManager().popBackStack();
            }

            @Override
            public void declined() {
                // nothing to do
            }
        });
    }

    private void saveNote() {
        presenter.addNote(
                getNote()
        );
    }

    public void setPresenter(NotesContract.Presenter presenter) {
        this.presenter = presenter;
    }

    private Note getNote() {
        if (mNote == null){
            // create the object, that we want to store
            mNote = new Note();
        }

        mNote.setTitle(editTextTitle.getEditableText().toString());

        if (editTextMemo.getEditableText().length() > 0) {
            mNote.setMemoText(editTextMemo.getEditableText().toString());
        }

        mNote.setLastEdited(System.currentTimeMillis());

        return mNote;
    }
}

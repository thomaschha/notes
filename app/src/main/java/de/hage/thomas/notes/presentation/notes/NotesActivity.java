package de.hage.thomas.notes.presentation.notes;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import de.hage.thomas.notes.Injection;
import de.hage.thomas.notes.Navigator;
import de.hage.thomas.notes.R;
import de.hage.thomas.notes.model.Note;
import de.hage.thomas.notes.presentation.UserConfirmation;
import de.hage.thomas.notes.presentation.notes.adapters.NotesPagerAdapter;
import de.hage.thomas.notes.presentation.notes.fragments.DoneFragment;
import de.hage.thomas.notes.presentation.notes.fragments.NoteFragment;
import de.hage.thomas.notes.presentation.notes.fragments.OverviewFragment;

/**
 * Created by tomes on 14.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class NotesActivity extends FragmentActivity implements NotesContract.View {

    private NotesContract.Presenter presenter;

    private EditText searchText;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        this.initialize();
    }

    private void initialize() {
        NotesPagerAdapter adapter = new NotesPagerAdapter(getSupportFragmentManager());

        // setup fragments
        OverviewFragment overviewFragment = OverviewFragment.getInstance();
        DoneFragment doneFragment = DoneFragment.getInstance();
        adapter.add(1, overviewFragment);
        adapter.add(0, doneFragment);

        // setup ViewPager
        final ViewPager viewPager = findViewById(R.id.activity_notes_viewpager);
        viewPager.setAdapter(adapter);

        // setup TabLayout
        TabLayout tabLayout = findViewById(R.id.activity_notes_tablayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setText(R.string.bin_title);
        tabLayout.getTabAt(1).setText(R.string.overview_title);
        tabLayout.getTabAt(1).select();

        // setup FAB
        final FloatingActionButton floatingActionButton = findViewById(R.id.activity_notes_fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.getEditableText().clear();
                switch (viewPager.getCurrentItem()) {
                    case 0: // bin fragment
                        presenter.emptyBin();
                        return;
                    case 1: // overview fragment
                        presenter.requestFragmentNewNote();
                        return;
                }
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    floatingActionButton.setImageResource(R.drawable.ic_bin);
                } else {
                    floatingActionButton.setImageResource(R.drawable.ic_add);
                }
                floatingActionButton.show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (ViewPager.SCROLL_STATE_IDLE == state) {
                    floatingActionButton.show();
                } else {
                    floatingActionButton.hide();
                }
            }
        });

        // setup SearchView
        searchText = findViewById(R.id.activity_notes_searchview);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.setFiltering(s);
            }
        });

        // setup .Presenter
        presenter = new NotesPresenter(
                Injection.getRepository(this),
                this,
                overviewFragment,
                doneFragment
        );
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Navigator.onSaveInstanceState(getSupportFragmentManager());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void showNothingToRecycleToast() {
        Toast.makeText(this, R.string.toast_nothing_to_recycle, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNoteFragmentForNewNote() {
        Navigator.showNewNoteFragment(getSupportFragmentManager(), presenter);
    }

    @Override
    public void showEmptyBinDialog(final UserConfirmation user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
                .setTitle(R.string.dialog_confirm_delete_all_title)
                .setMessage(R.string.dialog_confirm_delete_all_msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        user.confirmed();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        user.declined();
                        dialog.dismiss();
                    }
                }).create();

        builder.show();
    }

    @Override
    public void showNoteDeleteDialog(final UserConfirmation callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
                .setTitle(R.string.dialog_confirm_delete_title)
                .setMessage(R.string.dialog_confirm_delete_msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.confirmed();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.declined();
                        dialog.dismiss();
                    }
                }).create();

        builder.show();
    }

    @Override
    public void showNoteFragmentForNote(Note note) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(NoteFragment.BUNDLE_KEY_OPENED_NOTE, note);
        Navigator.showNoteFragmentForNote(getSupportFragmentManager(), presenter, bundle);
    }
}

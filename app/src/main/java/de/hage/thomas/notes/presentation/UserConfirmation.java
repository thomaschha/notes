package de.hage.thomas.notes.presentation;

/**
 * Created by tomes on 14.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface UserConfirmation {
    void confirmed();
    void declined();
}
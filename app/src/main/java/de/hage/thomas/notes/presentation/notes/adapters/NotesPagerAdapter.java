package de.hage.thomas.notes.presentation.notes.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

import java.util.HashMap;

/**
 * Created by tomes on 14.11.17.
 */
@SuppressWarnings("DefaultFileTemplate")
public class NotesPagerAdapter extends FragmentPagerAdapter {

    private final SparseArray<Fragment> fragment;

    public NotesPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fragment = new SparseArray<>(2);
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragment.get(position);
    }

    @Override
    public int getCount() {
        return this.fragment.size();
    }

    public void add(int position, Fragment fragment) {
        this.fragment.put(position, fragment);
    }
}

package de.hage.thomas.notes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import de.hage.thomas.notes.presentation.notes.NotesContract;
import de.hage.thomas.notes.presentation.notes.fragments.NoteFragment;

/**
 * Created by tomes on 14.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class Navigator {
    public static void showNewNoteFragment(FragmentManager supportFragmentManager, NotesContract.Presenter presenter) {
        NoteFragment noteFragment = NoteFragment.getInstance();
        noteFragment.setPresenter(presenter);
        openFragmentFromRight(supportFragmentManager, noteFragment);
    }

    public static void showNoteFragmentForNote(FragmentManager supportFragmentManager, NotesContract.Presenter presenter, Bundle bundle) {
        NoteFragment noteFragment = NoteFragment.getInstance();
        noteFragment.setPresenter(presenter);
        noteFragment.setArguments(bundle);
        openFragmentFromRight(supportFragmentManager, noteFragment);
    }

    private static void openFragmentFromRight(FragmentManager supportFragmentManager, Fragment fragment){
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_fragment, R.anim.exit_fragment, R.anim.enter_fragment, R.anim.exit_fragment);
        fragmentTransaction.add(
                R.id.activity_notes_root,
                fragment,
                fragment.getTag()
        );
        fragmentTransaction.addToBackStack(null)
                .commit();
    }

    public static void onSaveInstanceState(FragmentManager supportFragmentManager) {
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        for (Fragment f : supportFragmentManager.getFragments()){
            fragmentTransaction.remove(f);
        }
        fragmentTransaction.commit();
    }
}

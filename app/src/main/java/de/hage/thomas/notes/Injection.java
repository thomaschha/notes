package de.hage.thomas.notes;

import android.content.Context;

import de.hage.thomas.notes.data.ContractRepository;
import de.hage.thomas.notes.data.DataAccessObject;
import de.hage.thomas.notes.data.NotesRepository;

/**
 * Created by tomes on 14.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class Injection {
    public static ContractRepository.Notes getRepository(Context context) {
        return new NotesRepository(
                new DataAccessObject(context)
        );
    }
}

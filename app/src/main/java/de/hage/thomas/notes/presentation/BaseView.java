package de.hage.thomas.notes.presentation;

/**
 * Every view needs a method for setting the presenter
 * Created by tomes on 14.11.17.
 */
public interface BaseView<P> {
    void setPresenter(P presenter);
    boolean attached();
}

package de.hage.thomas.notes.presentation.notes.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hage.thomas.notes.R;
import de.hage.thomas.notes.model.Note;

/**
 * Created by tomes on 14.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class NotesRecyclerAdapter extends RecyclerView.Adapter<NotesRecyclerAdapter.ViewHolder> {

    private List<Note> notes = new ArrayList<>();
    private OnNoteSharedListener sharedListener;
    private OnItemClickListener clickListener;
    private List<Note> copyNotes;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recyclerview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Note note = notes.get(position);
        if (note != null) {
            holder.title.setText(note.getTitle());
            holder.date.setText(note.getLastEditedText());
            holder.memo.setText(note.getMemoText());
            holder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    informSharedListeners(note);
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    informClickListeners(note);
                }
            });
        }
    }

    private void informClickListeners(Note note) {
        if (this.clickListener == null) return;
        this.clickListener.onNoteSelected(note);
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public void setNotes(List<Note> noteList) {
        this.notes.clear();
        this.notes.addAll(noteList);
    }

    public boolean hasItems() {
        return notes != null && notes.size() > 0;
    }

    public void addNote(Note note) {
        notes.add(0, note);
    }

    public void removeNote(int index) {
        notes.remove(index);
    }

    public int getItemIndex(Note oldNote) {
        for (int i = 0; i < notes.size(); i++) {
            if (notes.get(i).equals(oldNote)) {
                return i;
            }
        }
        return -1;
    }

    public void updateNoteAt(int index, Note updatedNote) {
        notes.remove(index);
        notes.add(index, updatedNote);
    }

    private void informSharedListeners(Note note) {
        if (this.sharedListener == null) return;
        this.sharedListener.onNoteShared(note);
    }

    public void setOnNoteSharedListener(OnNoteSharedListener listener) {
        if (listener == null) return;
        this.sharedListener = listener;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        if (listener == null) return;
        this.clickListener = listener;
    }

    public Note getItem(int position) {
        return this.notes.get(position);
    }

    public void clear() {
        this.notes.clear();
        this.notes = new ArrayList<>();
    }

    public void filter(String charSequence) {
        charSequence = charSequence.toLowerCase();
        if (this.copyNotes == null) this.copyNotes = this.notes;
        if (charSequence.length() == 0) {
            this.notes = this.copyNotes;
            this.copyNotes = null;
        } else {
            List<Note> result = new ArrayList<>();
            for (Note note : this.copyNotes) {
                if (note.getTitle() != null &&
                        note.getTitle().toLowerCase().contains(charSequence)) {
                    result.add(note);
                    continue;
                }
                if (note.getMemoText() != null &&
                        note.getMemoText().toLowerCase().contains(charSequence)) {
                    result.add(note);
                }
            }
            this.notes = result;
        }
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onNoteSelected(Note note);
    }

    public interface OnNoteSharedListener {
        void onNoteShared(Note note);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, date, memo;
        ImageButton share;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_title);
            date = itemView.findViewById(R.id.item_date);
            memo = itemView.findViewById(R.id.item_content);
            share = itemView.findViewById(R.id.item_share);
        }

    }
}

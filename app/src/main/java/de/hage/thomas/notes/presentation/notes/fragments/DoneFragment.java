package de.hage.thomas.notes.presentation.notes.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import de.hage.thomas.notes.R;
import de.hage.thomas.notes.model.Note;
import de.hage.thomas.notes.presentation.notes.NotesContract;
import de.hage.thomas.notes.presentation.notes.adapters.NotesRecyclerAdapter;

/**
 * Created by tomes on 14.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class DoneFragment extends Fragment implements NotesContract.View.Bin {

    private NotesContract.Presenter presenter;

    private NotesRecyclerAdapter adapter;

    private RecyclerView recyclerView;
    private ViewGroup emptyContainer;
    private ProgressBar progressBar;

    public static DoneFragment getInstance() {
        return new DoneFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_done, container, false);
        emptyContainer = v.findViewById(R.id.fragment_bin_container_empty_content);
        progressBar = v.findViewById(R.id.fragment_bin_progressbar);
        recyclerView = v.findViewById(R.id.fragment_bin_recyclerview);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new NotesRecyclerAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                presenter.setNoteOpen(
                        adapter.getItem(
                                viewHolder.getAdapterPosition()
                        )
                );
            }
        }).attachToRecyclerView(recyclerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.loadFinishedNotes();
    }

    @Override
    public void setPresenter(NotesContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public boolean attached() {
        return this.isAdded();
    }

    @Override
    public void onDoneNotesLoaded(List<Note> notes) {
        adapter.setNotes(notes);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showLoadingProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void hideRecyclerView() {
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void showRecyclerView() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmptyContentMessage() {
        emptyContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyContentMessage() {
        emptyContainer.setVisibility(View.GONE);
    }

    @Override
    public boolean hasItems() {
        return adapter.hasItems();
    }

    @Override
    public void removeAllNotes() {
        adapter.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void addDoneNote(Note note) {
        adapter.addNote(note);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onNoteRestored(Note toRemove) {
        int index = adapter.getItemIndex(toRemove);
        adapter.removeNote(index);
        adapter.notifyItemRemoved(
                index
        );
    }

    @Override
    public void applyFilter(CharSequence charSequence) {
        adapter.filter(charSequence.toString());
    }
}

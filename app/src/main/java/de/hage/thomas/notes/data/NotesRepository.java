package de.hage.thomas.notes.data;

import java.util.List;

import de.hage.thomas.notes.model.Note;

/**
 * Created by tomes on 14.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class NotesRepository implements ContractRepository.Notes {

    private final DataAccessObject dataAccessObject;

    private List<Note> cachedOpenTasks;
    private List<Note> cachedDoneTasks;
    private boolean updateNeeded;

    public NotesRepository(DataAccessObject dataAccessObject) {
        super();
        this.dataAccessObject = dataAccessObject;
        this.updateNeeded = false;
    }

    @Override
    public void resumeOpenTaskFragment(ContractRepository.DefaultCallback<List<Note>> callback) {
        if (updateNeeded) {
            cachedOpenTasks = dataAccessObject.getOpenTasks();
        }

        if (cachedOpenTasks == null) {
            cachedOpenTasks = dataAccessObject.getOpenTasks();
        }

        if (cachedOpenTasks.size() < 1) {
            // empty list, so assume, that this is an error.
            callback.onError();
            return;
        }
        // return cached tasks
        callback.onSuccess(cachedOpenTasks);
    }

    @Override
    public void resumeDoneTaskFragment(ContractRepository.DefaultCallback<List<Note>> callback) {
        if (updateNeeded) {
            updateNeeded = false;
            cachedDoneTasks = dataAccessObject.getDoneTasks();
            // TODO: We could spot any difference from view to database here.
        }

        if (cachedDoneTasks == null) {
            cachedDoneTasks = dataAccessObject.getDoneTasks();
        }

        if (cachedDoneTasks.size() < 1) {
            // empty -> nothing to show
            callback.onError();
            return;
        }
        // reply instantly
        callback.onSuccess(cachedDoneTasks);
    }

    @Override
    public void deleteDoneNotesFromDatabase() {
        dataAccessObject.deleteNotesFromDatabase();

        if (cachedDoneTasks != null) {
            cachedDoneTasks.clear();
        }
    }

    @Override
    public void markForBin(Note note) {
        dataAccessObject.markForBin(note);

        if (cachedDoneTasks == null){
            // something is wrong, apply a new loading next time resuming a fragment
            updateNeeded = true;
            // add this item to cached tasks
            cachedDoneTasks = dataAccessObject.getDoneTasks();
        }
        cachedDoneTasks.add(note);

        if (cachedOpenTasks == null){
            // similar to done tasks part
            updateNeeded = true;
            cachedOpenTasks = dataAccessObject.getOpenTasks();
        }
        cachedOpenTasks.remove(note);
    }

    @Override
    public void addNote(Note note) {
        dataAccessObject.addNote(note);

        if (cachedOpenTasks == null){
            // something is wrong, apply reloading next time the fragment resumes
            updateNeeded = true;
            cachedOpenTasks = dataAccessObject.getOpenTasks();
        }
        cachedOpenTasks.add(note);
    }

    @Override
    public void setNoteOpen(Note toUpdate) {
        dataAccessObject.setNoteOpen(toUpdate);

        if (cachedOpenTasks == null){
            updateNeeded = true;
            cachedOpenTasks = dataAccessObject.getOpenTasks();
        }
        cachedOpenTasks.add(toUpdate);

        if (cachedDoneTasks == null){
            updateNeeded = true;
            cachedDoneTasks = dataAccessObject.getDoneTasks();
        }
        cachedDoneTasks.remove(toUpdate);
    }

    @Override
    public void setDefaultDummyItems() {
        dataAccessObject.setDefaultDummyItems();
    }
}

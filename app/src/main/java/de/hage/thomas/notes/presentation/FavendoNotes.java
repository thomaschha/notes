package de.hage.thomas.notes.presentation;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import de.hage.thomas.notes.BuildConfig;
import de.hage.thomas.notes.Injection;
import de.hage.thomas.notes.presentation.notes.NotesActivity;

/**
 * Created by tomes on 15.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class FavendoNotes extends AppCompatActivity {

    private final String savedVersion_key = "log_first_startup_key";

    @SuppressLint("ApplySharedPref")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int currentVersion = BuildConfig.VERSION_CODE;
        SharedPreferences defaultPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (defaultPreferences.getInt(savedVersion_key, -1) == -1){
            defaultPreferences.edit().putInt(
                    savedVersion_key,
                    currentVersion
            ).commit();
            Injection.getRepository(this)
                    .setDefaultDummyItems();
        }
        startActivity(new Intent(this, NotesActivity.class));
    }
}

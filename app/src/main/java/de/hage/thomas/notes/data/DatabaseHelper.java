package de.hage.thomas.notes.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by tomes on 15.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "NOTES";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context){
        this(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DataAccessObject.NOTES_DATABASE.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

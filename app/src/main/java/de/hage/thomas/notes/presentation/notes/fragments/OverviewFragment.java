package de.hage.thomas.notes.presentation.notes.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import de.hage.thomas.notes.R;
import de.hage.thomas.notes.model.Note;
import de.hage.thomas.notes.presentation.notes.NotesContract;
import de.hage.thomas.notes.presentation.notes.adapters.NotesRecyclerAdapter;
import de.hage.thomas.notes.presentation.utils.StringUtil;

/**
 * Created by tomes on 14.11.17.
 */
@SuppressWarnings("DefaultFileTemplate")
public class OverviewFragment extends Fragment implements NotesContract.View.Overview, NotesRecyclerAdapter.OnNoteSharedListener {

    private NotesContract.Presenter presenter;

    private NotesRecyclerAdapter adapter;

    private RecyclerView recyclerView;
    private ViewGroup emptyContainer;
    private ProgressBar progressBar;


    public static OverviewFragment getInstance() {
        return new OverviewFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_overview, container, false);
        emptyContainer = v.findViewById(R.id.fragment_overview_container_empty_content);
        progressBar = v.findViewById(R.id.fragment_overview_progressbar);
        recyclerView = v.findViewById(R.id.fragment_overview_recyclerview);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new NotesRecyclerAdapter();
        adapter.setOnNoteSharedListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT){

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                presenter.setNoteDone(
                        adapter.getItem(
                                viewHolder.getAdapterPosition()
                        )
                );
            }
        }).attachToRecyclerView(recyclerView);
        adapter.setOnItemClickListener(new NotesRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onNoteSelected(Note note) {
                presenter.onNoteSelected(note);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.loadActiveNotes();
    }

    @Override
    public void setPresenter(NotesContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public boolean attached() {
        return this.isAdded();
    }

    @Override
    public void onOpenNotesLoaded(List<Note> notes) {
        adapter.setNotes(notes);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showLoadingProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showRecyclerView() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRecyclerView() {
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyContentMessage() {
        emptyContainer.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideEmptyContentMessage() {
        emptyContainer.setVisibility(View.GONE);
    }

    @Override
    public void removeDoneNote(Note note) {
        int index = adapter.getItemIndex(note);
        adapter.removeNote(index);
        adapter.notifyItemRemoved(
                index
        );
    }

    @Override
    public void addOpenNote(Note note) {
        int index = adapter.getItemIndex(note);
        if (index >= 0){
            adapter.updateNoteAt(index, note);
        } else {
            adapter.addNote(note);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void applyFilter(CharSequence charSequence) {
        adapter.filter(charSequence.toString());
    }

    @Override
    public boolean hasItems() {
        return adapter.hasItems();
    }

    @Override
    public void onNoteShared(Note note) {
        // this is foreign code from:
        // https://faq.whatsapp.com/en/android/28000012?lang=de
        Intent sender = new Intent(Intent.ACTION_SEND);
        sender.putExtra(
                Intent.EXTRA_TEXT,
                StringUtil.formatNoteToMessage(getContext(), note)
                );
        sender.setType("text/plain");
        startActivity(sender);
    }
}

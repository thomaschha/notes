package de.hage.thomas.notes.presentation.utils;

import android.content.Context;
import android.text.format.DateUtils;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.hage.thomas.notes.R;
import de.hage.thomas.notes.model.Note;

/**
 * Created by tomes on 14.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class StringUtil {
    public static String formatNoteToMessage(Context context, Note note) {
        return String.format(context.getString(R.string.share_msg), note.getTitle(), note.getMemoText());
    }

    public static String formatLongToString(long lastEdited) {
        return new SimpleDateFormat("dd.MM.yyyy hh:mm", Locale.ENGLISH).format(new Date(lastEdited));
    }
}

package de.hage.thomas.notes.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import de.hage.thomas.notes.model.Note;

/**
 * Created by tomes on 15.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class DataAccessObject {

    private final DatabaseHelper dbHelper;

    public DataAccessObject(Context context) {
        super();
        this.dbHelper = new DatabaseHelper(context);
    }

    List<Note> getOpenTasks() {
        List<Note> result = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cur = db.query(
                NOTES_DATABASE.TABLE_NAME,
                new String[]{
                        NOTES_DATABASE.ID,
                        NOTES_DATABASE.TITLE,
                        NOTES_DATABASE.MEMO,
                        NOTES_DATABASE.DATE
                },
                "open = ?",
                new String[]{
                        NOTES_DATABASE.CONSTRAINT_IS_OPEN
                },
                null, null,
                "date DESC"
        );

        while (cur.moveToNext()){
            Note note = new Note();
            note.setID(cur.getLong(0));
            note.setTitle(cur.getString(1));
            note.setMemoText(cur.getString(2));
            note.setLastEdited(cur.getLong(3));
            result.add(note);
        }

        if (cur != null) {
            cur.close();
        }

        db.close();

        return result;
    }

    List<Note> getDoneTasks() {
        List<Note> result = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cur = db.query(
                NOTES_DATABASE.TABLE_NAME,
                new String[]{
                        NOTES_DATABASE.ID,
                        NOTES_DATABASE.TITLE,
                        NOTES_DATABASE.MEMO,
                        NOTES_DATABASE.DATE
                },
                "open = ?",
                new String[]{
                        NOTES_DATABASE.CONSTRAINT_IS_DONE
                },
                null, null,
                "date DESC"
        );

        while (cur.moveToNext()){
            Note note = new Note();
            note.setID(cur.getLong(0));
            note.setTitle(cur.getString(1));
            note.setMemoText(cur.getString(2));
            note.setLastEdited(cur.getLong(3));
            result.add(note);
        }

        if (cur != null) cur.close();

        db.close();
        return result;
    }

    void deleteNotesFromDatabase() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(
                NOTES_DATABASE.TABLE_NAME,
                "open = ?",
                new String[]{
                        NOTES_DATABASE.CONSTRAINT_IS_OPEN
                }
        );

        db.close();
    }

    void markForBin(Note note) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues cv = prepareContentValues(note);
        cv.put(NOTES_DATABASE.OPEN, NOTES_DATABASE.CONSTRAINT_IS_DONE);

        if (note.getID() > 0){
            // update
            db.update(
                    NOTES_DATABASE.TABLE_NAME,
                    cv, "id = ?",
                    new String[]{ String.valueOf(note.getID())}
            );
        } else {
            // insert
            db.insert(NOTES_DATABASE.TABLE_NAME, null, cv);
        }

        db.close();
    }

    void addNote(Note note) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues cv = prepareContentValues(note);
        cv.put(NOTES_DATABASE.OPEN, NOTES_DATABASE.CONSTRAINT_IS_OPEN);

        if (note.getID() > 0){
            // update
            db.update(NOTES_DATABASE.TABLE_NAME, cv, "id = ?",
                    new String[]{ String.valueOf(note.getID())});
        } else {
            // insert
            db.insert(NOTES_DATABASE.TABLE_NAME, null, cv);
        }

        db.close();
    }

    void setNoteOpen(Note note) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues cv = prepareContentValues(note);
        cv.put(NOTES_DATABASE.OPEN, NOTES_DATABASE.CONSTRAINT_IS_OPEN);

        if (note.getID() > 0){
            // update
            db.update(
                    NOTES_DATABASE.TABLE_NAME, cv,
                    "id = ?", new String[]{ String.valueOf(note.getID())}
            );
        } else {
            // insert
            db.insert(NOTES_DATABASE.TABLE_NAME, null, cv);
        }
        db.close();
    }

    private ContentValues prepareContentValues(Note note){
        ContentValues cv = new ContentValues();
        cv.put(NOTES_DATABASE.TITLE, note.getTitle());
        cv.put(NOTES_DATABASE.MEMO, note.getMemoText());
        cv.put(NOTES_DATABASE.DATE, note.getLastEdited());
        return cv;
    }

    public void setDefaultDummyItems() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String table = NOTES_DATABASE.TABLE_NAME;

        db.insert(table, null, getBinDummy("Lorem ipsum dolor sit amet", "..., consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "));
        db.insert(table, null, getBinDummy(" At vero eos et accusam et justo duo dolores et ea rebum.", " Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."));
        db.insert(table, null, getBinDummy("Lorem ipsum dolor", " sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "));
        db.insert(table, null, getBinDummy("Recycle a Note?", "You can do this by swiping this card to the right"));

        db.insert(table, null, getNoteDummy("Did you got it the first time?!", "Restore a note from the bin"));
        db.insert(table, null, getNoteDummy("Empty Messages or Titles?", ""));
        db.insert(table, null, getNoteDummy("Swipe your note", "This way you will put it in the bin"));
        db.insert(table, null, getNoteDummy("Don't forget the milk!", "If milk isn't available, grab a beer instead!"));

        db.close();
    }

    private ContentValues getBinDummy(String title, String memo){
        ContentValues cv = new ContentValues();
        cv.put(NOTES_DATABASE.TITLE, title);
        cv.put(NOTES_DATABASE.MEMO, memo);
        cv.put(NOTES_DATABASE.DATE, System.currentTimeMillis());
        cv.put(NOTES_DATABASE.OPEN, NOTES_DATABASE.CONSTRAINT_IS_DONE);
        return cv;
    }

    private ContentValues getNoteDummy(String title, String memo){
        ContentValues cv = new ContentValues();
        cv.put(NOTES_DATABASE.TITLE, title);
        cv.put(NOTES_DATABASE.MEMO, memo);
        cv.put(NOTES_DATABASE.DATE, System.currentTimeMillis());
        cv.put(NOTES_DATABASE.OPEN, NOTES_DATABASE.CONSTRAINT_IS_OPEN);
        return cv;
    }

    static class NOTES_DATABASE {

        private static final String TYPE_TEXT = " TEXT, ";
        private static final String TYPE_INTEGER = " INTEGER, ";

        private static final String TABLE_NAME = "notes";
        private static final String ID = "id";
        private static final String TITLE = "title";
        private static final String MEMO = "memo";
        private static final String DATE = "date";
        private static final String OPEN = "open";

        private static final String CONSTRAINT_IS_OPEN = "open";
        private static final String CONSTRAINT_IS_DONE = "done";

        static final String CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                        ID + " INTEGER PRIMARY KEY, " +
                        TITLE + TYPE_TEXT +
                        MEMO + TYPE_TEXT +
                        DATE + TYPE_INTEGER +
                        OPEN + " TEXT CHECK (open IN('" +
                        CONSTRAINT_IS_OPEN+"', '"+CONSTRAINT_IS_DONE+"')))";

    }
}

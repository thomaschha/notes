package de.hage.thomas.notes.model;

import android.os.Parcel;
import android.os.Parcelable;

import de.hage.thomas.notes.presentation.utils.StringUtil;

/**
 * Created by tomes on 14.11.17.
 */
@SuppressWarnings("DefaultFileTemplate")
public class Note implements Parcelable{

    private long lastEdited;
    private String title;
    private String memoText;
    private long ID;

    public Note() {
        super();
        title = "";
        memoText = "";
    }

    protected Note(Parcel in) {
        lastEdited = in.readLong();
        title = in.readString();
        memoText = in.readString();
        ID = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(lastEdited);
        dest.writeString(title);
        dest.writeString(memoText);
        dest.writeLong(ID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public long getLastEdited(){
        return lastEdited;
    }

    public String getLastEditedText() {
        return StringUtil.formatLongToString(lastEdited);
    }

    public String getMemoText() {
        return memoText;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMemoText(String memoText) {
        this.memoText = memoText;
    }

    public void setLastEdited(long date) {
        lastEdited = date;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }
}

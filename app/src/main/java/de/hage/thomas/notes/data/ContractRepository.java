package de.hage.thomas.notes.data;

import java.util.List;

import de.hage.thomas.notes.model.Note;

/**
 * Created by tomes on 14.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface ContractRepository {

    interface Notes {

        void resumeOpenTaskFragment(DefaultCallback<List<Note>> callback);

        void resumeDoneTaskFragment(DefaultCallback<List<Note>> callback);

        void deleteDoneNotesFromDatabase();

        void markForBin(Note note);

        void addNote(Note note);

        void setNoteOpen(Note toUpdate);

        void setDefaultDummyItems();
    }

    interface DefaultCallback<R> {
        void onSuccess(R object);
        void onError();
    }
}

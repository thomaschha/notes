package de.hage.thomas.notes.presentation.notes;

import java.util.List;

import de.hage.thomas.notes.data.ContractRepository;
import de.hage.thomas.notes.model.Note;
import de.hage.thomas.notes.presentation.UserConfirmation;

/**
 * Created by tomes on 14.11.17.
 */

@SuppressWarnings("DefaultFileTemplate")
class NotesPresenter implements NotesContract.Presenter {

    private final ContractRepository.Notes notesRepository;
    private final NotesContract.View activityView;
    private final NotesContract.View.Overview overview;
    private final NotesContract.View.Bin binView;

    public NotesPresenter(ContractRepository.Notes notesRepository, NotesContract.View activityView, NotesContract.View.Overview overview, NotesContract.View.Bin binView) {
        this.notesRepository = notesRepository;
        this.activityView = activityView;
        this.overview = overview;
        this.binView = binView;

        overview.setPresenter(this);
        binView.setPresenter(this);
    }

    @Override
    public void loadActiveNotes() {
        // inform about loading
        overview.hideEmptyContentMessage();
        overview.hideRecyclerView();
        overview.showLoadingProgressBar();

        notesRepository.resumeOpenTaskFragment(new ContractRepository.DefaultCallback<List<Note>>() {
            @Override
            public void onSuccess(List<Note> object) {
                if (object == null || object.size() < 1) {
                    overview.hideLoadingProgressBar();
                    overview.showEmptyContentMessage();
                    return;
                }
                overview.hideLoadingProgressBar();
                overview.showRecyclerView();
                // we have at least one note in the list, so publish
                overview.onOpenNotesLoaded(object);
            }

            @Override
            public void onError() {
                overview.hideLoadingProgressBar();
                overview.showEmptyContentMessage();
            }
        });
    }

    @Override
    public void loadFinishedNotes() {
        // inform about loading
        binView.hideEmptyContentMessage();
        binView.hideRecyclerView();
        binView.showLoadingProgressBar();

        notesRepository.resumeDoneTaskFragment(new ContractRepository.DefaultCallback<List<Note>>() {
            @Override
            public void onSuccess(List<Note> object) {
                if (object == null || object.size() < 1) {
                    binView.hideLoadingProgressBar();
                    binView.showEmptyContentMessage();
                    return;
                }
                // we have at least one note, so publish
                binView.hideLoadingProgressBar();
                binView.showRecyclerView();
                binView.onDoneNotesLoaded(object);
            }

            @Override
            public void onError() {
                binView.hideLoadingProgressBar();
                binView.showEmptyContentMessage();
            }
        });
    }

    @Override
    public void emptyBin() {
        if (binView.hasItems()) {
            activityView.showEmptyBinDialog(new UserConfirmation() {
                @Override
                public void confirmed() {
                    // update the binView
                    notesRepository.deleteDoneNotesFromDatabase();
                    binView.removeAllNotes();
                    binView.hideRecyclerView();
                    binView.hideLoadingProgressBar();
                    binView.showEmptyContentMessage();
                }

                @Override
                public void declined() {
                    // no changes needed
                }
            });
        } else {
            activityView.showNothingToRecycleToast();
        }
    }

    @Override
    public void requestFragmentNewNote() {
        activityView.showNoteFragmentForNewNote();
    }

    @Override
    public void setNoteDone(Note note) {
        if (note == null) return;
        if (binView.hasItems()) {
            // origin has still items left

            // inform our db
            notesRepository.markForBin(note);
            // add note to binView
            binView.addDoneNote(note);
            // remove note from origin
            overview.removeDoneNote(note);
            if (!overview.hasItems()) {
                // overview is now empty
                overview.hideRecyclerView();
                overview.showEmptyContentMessage();
            }
        } else {
            // update bin view, because it is empty

            binView.hideLoadingProgressBar();
            binView.hideEmptyContentMessage();
            binView.showRecyclerView();
            // tell the dao
            notesRepository.markForBin(note);

            // update the bin view
            binView.addDoneNote(note);

            // update the overview
            overview.removeDoneNote(note);
            if (!overview.hasItems()) {
                // overview is now empty
                overview.hideRecyclerView();
                overview.showEmptyContentMessage();
            }
        }
    }

    @Override
    public void addNote(Note note) {
        if (note == null) return;
        if (note.getTitle().isEmpty() && note.getMemoText().isEmpty()) return;
        
        if (overview.hasItems()) {
            notesRepository.addNote(note);
            overview.addOpenNote(note);
        } else {
            // handle ui:
            overview.hideEmptyContentMessage();
            overview.hideLoadingProgressBar();
            overview.showRecyclerView();

            notesRepository.addNote(note);
            overview.addOpenNote(note);
        }
    }

    @Override
    public void requestDeleteDialog(UserConfirmation userConfirmation) {
        activityView.showNoteDeleteDialog(userConfirmation);
    }

    @Override
    public void setNoteOpen(Note item) {
        if (item == null) return;
        if (overview.hasItems()){
            notesRepository.setNoteOpen(item);
            overview.addOpenNote(item);
            binView.onNoteRestored(item);
            if (!binView.hasItems()){
                binView.hideRecyclerView();
                binView.showEmptyContentMessage();
            }
        } else {
            overview.hideEmptyContentMessage();
            overview.hideLoadingProgressBar();
            overview.showRecyclerView();

            notesRepository.setNoteOpen(item);
            overview.addOpenNote(item);
            binView.onNoteRestored(item);

            if (!binView.hasItems()){
                binView.hideRecyclerView();
                binView.showEmptyContentMessage();
            }
        }
    }

    @Override
    public void onNoteSelected(Note note) {
        if (note == null) return;
        activityView.showNoteFragmentForNote(note);
    }

    @Override
    public void setFiltering(CharSequence filterSequence) {
        if (overview.attached()) overview.applyFilter(filterSequence);
        if (binView.attached()) binView.applyFilter(filterSequence);
    }
}

package de.hage.thomas.notes.presentation.notes;

import java.util.List;

import de.hage.thomas.notes.model.Note;
import de.hage.thomas.notes.presentation.BaseView;
import de.hage.thomas.notes.presentation.UserConfirmation;

/** Contract for the communication between Presenter and View
 * Created by tomes on 14.11.17.
 */
public interface NotesContract {

    interface View {
        // implemented by the activity, dont need reference to presenter
        void showNothingToRecycleToast();

        void showNoteFragmentForNewNote();

        void showEmptyBinDialog(UserConfirmation user);

        void showNoteDeleteDialog(UserConfirmation callback);

        void showNoteFragmentForNote(Note note);

        interface Overview extends BaseView<Presenter> {

            void onOpenNotesLoaded(List<Note> notes);

            void showLoadingProgressBar();

            void hideLoadingProgressBar();

            void showRecyclerView();

            void hideRecyclerView();

            void showEmptyContentMessage();

            void hideEmptyContentMessage();

            void removeDoneNote(Note note);

            void addOpenNote(Note note);

            void applyFilter(CharSequence charSequence);

            boolean hasItems();
        }

        interface Bin extends BaseView<Presenter> {

            void onDoneNotesLoaded(List<Note> notes);

            void showLoadingProgressBar();

            void hideLoadingProgressBar();

            void hideRecyclerView();

            void showRecyclerView();

            void showEmptyContentMessage();

            void hideEmptyContentMessage();

            boolean hasItems();

            void removeAllNotes();

            void addDoneNote(Note note);

            void onNoteRestored(Note toRemove);

            void applyFilter(CharSequence charSequence);
        }
    }

    interface Presenter {

        void loadActiveNotes();

        void loadFinishedNotes();

        void emptyBin();

        void requestFragmentNewNote();

        void setNoteDone(Note note);

        void addNote(Note note);

        void requestDeleteDialog(UserConfirmation userConfirmation);

        void setNoteOpen(Note item);

        void onNoteSelected(Note note);

        void setFiltering(CharSequence filterSequence);
    }
}
